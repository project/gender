<?php

/**
 * @file
 * A textfield with associated data to collect gender information from users.
 */

/**
 * Implements hook_help().
 */
function gender_help($path, $arg) {
  if ($path == 'admin/help#gender') {
    $output = '';
    $output .= '<h3>' . t('About') . '</h3>';
    $output .= '<p>' . t('The gender field module provides inclusive options for collecting gender information of individuals.') . '</p>';
    $output .= '<blockquote>';
    $output .= '<p>' . t('This is not an area that we often have to give thought to, but it is also not complicated if we approach the user experience with empathy and consideration for the sensitive situations we may put our users in.') . '</p>';
    $output .= '<cite>' . t('<a href="@url">Asking for Gender in Applications</a> by Zoe Gagnon', array('@url' => 'https://builttoadapt.io/asking-for-gender-in-applications-687a3ba2cea8')) . '</cite>';
    $output .= '</blockquote>';
    $output .= '<p>' . t('See <a href="@project-page">the gender field module project page</a> after installing for more details and a guide.', array('@project-page' => 'https://www.drupal.org/project/gender')) . '</p>';

    return $output;
  }
}

/**
 * Implements hook_field_info().
 */
function gender_field_info() {
  $fields = array();

  $fields['gender'] = array(
    'label' => t('Gender'),
    'description' => t('This field stores gender in the database.'),
    'settings' => array(),
    'instance_settings' => array(),
    'default_widget' => 'gender_default',
    'default_formatter' => 'gender_default',
  );

  return $fields;
}

/**
 * Implements hook_field_is_empty().
 */
function gender_field_is_empty($item, $field) {
  return empty($item['part_1']) && empty($item['part_2']) && empty($item['part_3']) && empty($item['part_4']);
}

/**
 * Implements hook_field_widget_info().
 */
function gender_field_widget_info() {
  $widgets = array();

  $widgets['gender_default'] = array(
    'label' => t('Gender'),
    'field types' => array(
      'gender',
    ),
    'settings' => array(),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      'default value' => FIELD_BEHAVIOR_NONE,
    ),
  );

  return $widgets;
}

/**
 * Implements hook_field_widget_form().
 */
function gender_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  switch ($instance['widget']['type']) {
    case 'gender_default':
      $required = !empty($instance['required']);
      $gender_options = gender_options();
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
      $element['#element_validate'] = array(
        'gender_field_widget_validate',
      );
      // Set up a default value for the radio buttons - if the field is
      // required, it should default to NULL, if it isn't required, it should
      // default to the empty "- Select -" option.
      $default_radio_value = !empty($required) ? NULL : '';
      $element['part_1'] = array(
        '#type' => 'radios',
        '#title' => $gender_options['part_1']['label'],
        '#options' => $gender_options['part_1']['options'],
        '#default_value' => !empty($items[0]['part_1']) ? $items[0]['part_1'] : $default_radio_value,
        '#required' => $required,
      );
      $element['part_2'] = array(
        '#type' => 'radios',
        '#title' => $gender_options['part_2']['label'],
        '#options' => $gender_options['part_2']['options'],
        '#default_value' => !empty($items[0]['part_2']) ? $items[0]['part_2'] : $default_radio_value,
        '#required' => $required,
      );
      $element['part_3'] = array(
        '#type' => 'radios',
        '#title' => $gender_options['part_3']['label'],
        '#options' => $gender_options['part_3']['options'],
        '#default_value' => !empty($items[0]['part_3']) ? $items[0]['part_3'] : $default_radio_value,
        '#required' => $required,
      );
      // Add a default empty option to the radio button questions.
      if (!$required) {
        $optional_option = array(
          '' => t('- Select -'),
        );
        for ($i = 1; $i <= 3; $i++) {
          $element['part_' . $i]['#options'] = array_merge($optional_option, $element['part_' . $i]['#options']);
        }
      }
      $element['part_4'] = array(
        '#type' => 'select_or_other',
        '#title' => $gender_options['part_4']['label'],
        '#options' => $gender_options['part_4']['options'],
        '#multiple' => TRUE,
        '#select_type' => 'checkboxes',
        '#default_value' => !empty($items[0]['part_4']) ? explode(',', $items[0]['part_4']) : '',
        '#other' => t('Self Identify'),
        '#other_unknown_defaults' => 'other',
        '#other_delimiter' => FALSE,
        '#required' => $required,
      );
      break;
  }
  $element['#attached']['css'][] = drupal_get_path('module', 'gender') . '/gender.css';
  return $element;
}

/**
 * Form element validation callback for the default gender widget.
 */
function gender_field_widget_validate($element, &$form_state) {
  // Get the values for part 4 of the form.
  $part_4_values = array_keys($element['part_4']['select']['#value']);
  // See if the "other" option is checked.
  $other_position = array_search('select_or_other', $part_4_values);
  // If the other option is checked, and it has a value, add it to the list.
  if ($other_position !== FALSE) {
    if (!empty($element['part_4']['other']['#value'])) {
      $part_4_values[] = $element['part_4']['other']['#value'];
    }
    // Remove the "other option is checked" value from the list.
    unset($part_4_values[$other_position]);
    // Reset the keys on the array after using unset().
    $part_4_values = array_values($part_4_values);
  }
  // Create the final items array.
  $items = array(
    array(
      'part_1' => $element['part_1']['#value'],
      'part_2' => $element['part_2']['#value'],
      'part_3' => $element['part_3']['#value'],
      'part_4' => implode(',', $part_4_values),
    ),
  );
  // Set the value for the field.
  form_set_value($element, $items, $form_state);
}

/**
 * Implements hook_field_formatter_info().
 */
function gender_field_formatter_info() {
  $formatters = array();

  $formatters['gender_default'] = array(
    'label' => t('Default'),
    'field types' => array(
      'gender',
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_view().
 *
 * @throws \Exception
 *   The theme() function will throw an exception if it is called before all
 *   modules are loaded. That shouldn't ever happen here.
 *
 * @see theme()
 */
function gender_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'gender_default':
      $gender_options = gender_options();
      if (!empty($items[0]['part_1'])) {
        $element[0]['part_1'] = array(
          '#type' => 'item',
          '#title' => $gender_options['part_1']['label'],
          '#markup' => $gender_options['part_1']['options'][$items[0]['part_1']],
        );
      }
      if (!empty($items[0]['part_2'])) {
        $element[0]['part_2'] = array(
          '#type' => 'item',
          '#title' => $gender_options['part_2']['label'],
          '#markup' => $gender_options['part_2']['options'][$items[0]['part_2']],
        );
      }
      if (!empty($items[0]['part_3'])) {
        $element[0]['part_3'] = array(
          '#type' => 'item',
          '#title' => $gender_options['part_3']['label'],
          '#markup' => $gender_options['part_3']['options'][$items[0]['part_3']],
        );
      }
      if (!empty($items[0]['part_4'])) {
        // Get the individual values of part 4.
        $part_4_values = explode(',', $items[0]['part_4']);
        foreach ($part_4_values as &$part_4_item) {
          // If a value has a translation in the original options, use that
          // instead.
          if (!empty($gender_options['part_4']['options'][$part_4_item])) {
            $part_4_item = $gender_options['part_4']['options'][$part_4_item];
          }
        }
        $element[0]['part_4'] = array(
          '#type' => 'item',
          '#title' => $gender_options['part_4']['label'],
          '#markup' => theme('item_list', array(
            'type' => 'ul',
            'items' => $part_4_values,
          )),
        );
      }
  }

  return $element;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function gender_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if (!empty($form['#field']['type']) && $form['#field']['type'] == 'gender') {
    // Modify the "Help text" section.
    $form['instance']['description']['#title'] = t('Justification for asking for this information');
    $original_description = $form['instance']['description']['#description'];
    $description = t("This data should only be collected when needed, and you should provide justification to your users for why you are asking for it. For further information, consult the <a target='_blank' href='@url'>gender field module's project page</a>.<br>", array(
      '@url' => url('https://www.drupal.org/project/gender'),
    ));
    $form['instance']['description']['#description'] = '<strong>' . $description . '</strong>' . $original_description;
    // Hide the cardinality select field.
    $form['field']['cardinality']['#value'] = 1;
    $form['field']['cardinality']['#type'] = 'hidden';
  }
}

/**
 * Get the list of gender options.
 *
 * @return array
 *   An associative array of gender options for each part. Each part has two
 *   keys, 'label' and 'options'. Label is a translated string, options is an
 *   associative array with text keys and translatable labels, suitable for use
 *   as the options in a select element.
 */
function gender_options() {
  return array(
    'part_1' => array(
      'label' => t('Do you consider yourself to be transgender?'),
      'options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
        'questioning' => t('Questioning'),
      ),
    ),
    'part_2' => array(
      'label' => t('Do you consider yourself to be gender non-conforming, gender diverse, gender variant, or gender expansive?'),
      'options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
        'questioning' => t('Questioning'),
      ),
    ),
    'part_3' => array(
      'label' => t('Are you intersex?'),
      'options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
        'i_dont_know' => t("I don't know"),
      ),
    ),
    'part_4' => array(
      'label' => t('Where do you identify on the gender spectrum (check all that apply)?'),
      'options' => array(
        'woman' => t('Woman'),
        'demi-girl' => t('Demi-girl'),
        'man' => t('Man'),
        'demi-boy' => t('Demi-boy'),
        'non-binary' => t('Non-binary'),
        'demi-non-binary' => t('Demi-non-binary'),
        'genderqueer' => t('Genderqueer'),
        'genderflux' => t('Genderflux'),
        'genderfluid' => t('Genderfluid'),
        'demi-fluid' => t('Demi-fluid'),
        'demi-gender' => t('Demi-gender'),
        'bigender' => t('Bigender'),
        'trigender' => t('Trigender'),
        'two-spirit' => t('Two Spirit'),
        'multigender/polygender' => t('Multigender/polygender'),
        'pangender/omnigender' => t('Pangender/omnigender'),
        'maxigender' => t('Maxigender'),
        'aporagender' => t('Aporagender'),
        'intergender' => t('Intergender'),
        'maverique' => t('Maverique'),
        'gender-confusion/gender-f*ck' => t('Gender confusion/Gender f*ck'),
        'gender-indifferent' => t('Gender indifferent'),
        'graygender' => t('Graygender'),
        'agender/genderless' => t('Agender/genderless'),
        'demi-agender' => t('Demi-agender'),
        'genderless' => t('Genderless'),
        'gender-neutral' => t('Gender neutral'),
        'neutrois' => t('Neutrois'),
        'androgynous' => t('Androgynous'),
        'androgyne' => t('Androgyne'),
        'prefer-not-to-answer' => t('Prefer not to answer'),
      ),
    ),
  );
}
