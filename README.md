# INTRODUCTION
==============

The gender field module provides inclusive options for collecting gender
information of individuals.

>This is not an area that we often have to give thought to, but it is also not
>complicated if we approach the user experience with empathy and consideration
>for the sensitive situations we may put our users in.
>
[Asking for Gender in Applications][1] by Zoe Gagnon


# REQUIREMENTS
==============

This module requires the following modules:

* [Select (or other)][2]


# INSTALLATION
==============

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/documentation/install/modules-themes/modules-7 for
further information.


# CONFIGURATION
===============

See [the gender field module project page][3] after installing for more details
and a guide.

[1]: https://builttoadapt.io/asking-for-gender-in-applications-687a3ba2cea8
[2]: https://www.drupal.org/project/select_or_other
[3]: https://www.drupal.org/project/gender
